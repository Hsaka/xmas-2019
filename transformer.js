var fs = require('fs');

var data = fs.readFileSync('public/app.js', 'utf-8');

var newValue = data.replace(
  'var e="undefined"==typeof global?self:global;',
  'var brunchRequire = global.brunchRequire;'
);

newValue = newValue.replace(
  ',brunchRequire("___globals___"),brunchRequire("initialize");',
  ',global.brunchRequire("___globals___"),global.brunchRequire("initialize");'
);

fs.writeFileSync('public/app.js', newValue, 'utf-8');

console.log('app.js modified by transformer.');

// data = fs.readFileSync('public/vendor.js', 'utf-8');

// newValue = data.replace(
//   'var t="undefined"==typeof global?self:global;',
//   'var brunchRequire = global.brunchRequire;'
// );

// fs.writeFileSync('public/vendor.js', newValue, 'utf-8');

// console.log('vendor.js modified by transformer.');
