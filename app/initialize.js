import BootScene from 'scenes/boot';
import PreloaderScene from 'scenes/preloader';
import GameScene from 'scenes/game';
import ControllerScene from 'scenes/controller';
import Utils from 'utils/utils';
import VirtualJoyStickPlugin from 'plugins/rexvirtualjoystickplugin.min.js';

window.onload = function() {
  function resize() {
    var canvas = document.querySelector('canvas');
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = Utils.GlobalSettings.width / Utils.GlobalSettings.height;
    if (windowRatio < gameRatio) {
      canvas.style.width = windowWidth + 'px';
      canvas.style.height = windowWidth / gameRatio + 'px';
    } else {
      canvas.style.width = windowHeight * gameRatio + 'px';
      canvas.style.height = windowHeight + 'px';
    }
  }

  function inIframe() {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }

  Utils.setup();

  window.game = new Phaser.Game({
    // See <https://github.com/photonstorm/phaser/blob/master/src/boot/Config.js>

    width: Utils.GlobalSettings.width,
    height: Utils.GlobalSettings.height,
    // zoom: 1,
    // resolution: 1,
    type: Phaser.AUTO,
    // parent: null,
    // canvas: null,
    // canvasStyle: null,
    // seed: null,
    title: 'Snake 99', // 'My Phaser 3 Game'
    url: 'https://gamepyong.com',
    version: '0.0.1',
    input: {
      keyboard: true,
      mouse: true,
      touch: true,
      gamepad: false
    },
    disableContextMenu: true,
    // banner: false
    banner: {
      // hidePhaser: false,
      // text: 'white',
      background: ['#e54661', '#ffa644', '#998a2f', '#2c594f', '#002d40']
    },
    // fps: {
    //   min: 10,
    //   target: 60,
    //   forceSetTimeout: false,
    // },
    // pixelArt: false,
    // transparent: false,
    //clearBeforeRender: false,
    //backgroundColor: '#2d2d8d', // black
    loader: {
      // baseURL: '',
      path: '',
      maxParallelDownloads: 6
      // crossOrigin: 'anonymous',
      // timeout: 0
    },
    physics: {
      default: 'arcade',
      arcade: {}
    },
    scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      width: Utils.GlobalSettings.width,
      height: Utils.GlobalSettings.height
    },
    scene: [BootScene, PreloaderScene, GameScene, ControllerScene],
    plugins: {
      global: [
        {
          key: 'rexVirtualJoyStick',
          plugin: VirtualJoyStickPlugin,
          start: true
        }
      ]
    }
  });

  //resize();
  //window.addEventListener('resize', resize, false);

  if (inIframe()) {
    $('body').css({ background: 'transparent' });
  } else {
    $('body').css({ background: '#000' });
  }
};
