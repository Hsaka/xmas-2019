import Utils from 'utils/utils';
import Screen from '../entities/screen';

export default class GameScene extends Phaser.Scene {
  constructor() {
    super('gameScene');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    this.bg = this.add.image(0, 0, 'bg');
    this.bg.setOrigin(0);
    this.bg.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bg.setTint(0xf39c12);
    this.bg.visible = false;
    this.bg.alpha = 0;

    this.setupSnow();

    this.input.on('pointerdown', () => {
      if (!this.started) {
        this.start();
      } else {
        console.log(this.songTime.getElapsed());
      }
    });

    this.keepAlive = 0;

    this.server = new HFT.GameServer();

    this.screenId = 0;
    this.screens = [];
    // A new player has arrived.
    this.server.on('playerconnect', netPlayer => {
      this.screens.push(new Screen(this, netPlayer, this.screenId));
      this.screenId++;
    });

    this.started = false;
    this.timeText = this.add.text(32, 32);
    this.timeText.visible = false;

    this.beatPoints = {};

    this.setupScene1();
    this.setupScene4();
    this.setupScene5();
    this.setupScene6();
    this.setupScene7();
    this.setupScene9();

    console.log(
      Utils.GlobalSettings.localIPAddress + ':' + Utils.GlobalSettings.port
    );
  }

  setupScene1() {
    this.bulb = this.add.sprite(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'atlas1',
      'light3'
    );

    this.bulb.visible = false;
  }

  setupScene4() {
    this.gift1 = this.add.sprite(0, 0, 'atlas1', 'gift1');
    this.gift2 = this.add.sprite(0, 0, 'atlas1', 'gift2');
    this.gift3 = this.add.sprite(0, 0, 'atlas1', 'gift3');
    this.gift4 = this.add.sprite(0, 0, 'atlas1', 'gift4');

    this.gift1.visible = false;
    this.gift2.visible = false;
    this.gift3.visible = false;
    this.gift4.visible = false;
  }

  setupScene5() {
    this.tree = this.add.image(0, 0, 'atlas1', 'tree');
    this.tree.visible = false;
  }

  setupScene6() {
    this.showStockings = false;
    this.stockingCircle = new Phaser.Geom.Circle(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      350
    );

    this.stockingGroup = this.add.group({
      key: 'atlas1',
      frame: ['stocking1', 'stocking2', 'stocking3'],
      repeat: 5
    });

    Phaser.Actions.PlaceOnCircle(
      this.stockingGroup.getChildren(),
      this.stockingCircle
    );

    this.stockingGroup.setVisible(false);
  }

  setupScene7() {
    this.santa = this.add.image(0, 0, 'atlas1', 'santa');
    this.santa.visible = false;
  }

  setupScene9() {
    this.merry = this.add.image(
      Utils.GlobalSettings.width / 2,
      150,
      'atlas1',
      'merry'
    );

    this.merry.visible = false;
  }

  setupSnow() {
    this.snowFall = false;

    this.snowPool = this.add.group();
    for (var i = 0; i < 20; i++) {
      var spr = this.snowPool.create(0, 0, 'atlas1', 'flake1');
      spr.alpha = Phaser.Math.RND.realInRange(0.5, 1);
      spr._xspeed = Phaser.Math.RND.realInRange(-0.3, 0.3);
      spr._yspeed = Phaser.Math.RND.realInRange(1.2, 1.4);
      spr._curYSpeed = 0;
      spr.setFrame('flake' + Phaser.Math.RND.integerInRange(1, 9));
      spr.setScale(Phaser.Math.RND.realInRange(0.3, 1));
      spr.x = Phaser.Math.RND.integerInRange(0, Utils.GlobalSettings.width);

      if (i < 10) {
        spr.y = Phaser.Math.RND.integerInRange(-100, -200);
      } else {
        spr.y = Phaser.Math.RND.integerInRange(
          -200,
          -Utils.GlobalSettings.height
        );
      }
    }
    this.snowPool.setVisible(false);
  }

  updateSnow(sprite) {
    if (sprite) {
      if (sprite.y > Utils.GlobalSettings.height + 100) {
        sprite.x = Phaser.Math.RND.integerInRange(
          0,
          Utils.GlobalSettings.width
        );

        sprite.y = Phaser.Math.RND.integerInRange(
          -100,
          -Utils.GlobalSettings.height
        );

        sprite.setFrame('flake' + Phaser.Math.RND.integerInRange(1, 9));
        sprite.alpha = Phaser.Math.RND.realInRange(0.5, 1);
        sprite._xspeed = Phaser.Math.RND.realInRange(-0.3, 0.3);
        sprite._yspeed = Phaser.Math.RND.realInRange(1.2, 1.4);
        sprite.setScale(Phaser.Math.RND.realInRange(0.3, 1));
      } else {
        sprite.x += sprite._xspeed;
        if (sprite.alpha > 0) {
          sprite.alpha -= 0.0002;
        }

        sprite.angle += sprite._xspeed;
        if (sprite.angle > 360) {
          sprite.angle = 0;
        }

        if (sprite._curYSpeed < sprite._yspeed) {
          sprite._curYSpeed += 0.01;
        } else {
          sprite._curYSpeed = sprite._yspeed;
        }

        sprite.y += sprite._curYSpeed;
      }
    }
  }

  showSnow() {
    this.snowFall = true;
    this.snowPool.setVisible(true);
  }

  showScene1() {
    this.cameras.main.flash(5000, 243, 156, 18);
    this.bg.alpha = 0;

    this.bulb.setFrame('light3');
    this.bulb.visible = true;
    this.bulb.setScale(0);
    this.bulb.alpha = 0;

    this.tweens.add({
      targets: [this.bulb],
      scaleX: 1,
      scaleY: 1,
      alpha: 1,
      ease: 'Back.easeOut',
      duration: 2500,
      onComplete: () => {
        this.bulb.setFrame('light6');
        this.tweens.add({
          targets: [this.bulb],
          alpha: 0,
          ease: 'Linear.easeOut',
          duration: 2500
        });
      }
    });
  }

  showScene2() {
    this.cameras.main.flash(200, 243, 156, 18, true);

    this.time.addEvent({
      delay: 200,
      callback: () => {
        this.cameras.main.flash(200, 243, 156, 18, true);
      },
      callbackScope: this,
      repeat: 24
    });
  }

  showScene3() {
    this.bg.setTint(0x3498db);
    this.bg.visible = true;
    this.bg.alpha = 0;
    this.bg.setOrigin(0.5, 0);
    this.bg.x = Utils.GlobalSettings.width / 2;
    var startingScale = this.bg.scaleX;
    this.bg.scaleX = 0;

    this.tweens.add({
      targets: [this.bg],
      alpha: 1,
      scaleX: startingScale,
      ease: 'Sine.easeOut',
      duration: 2000,
      onComplete: () => {
        this.sendTo(2, 'doScene', { scene: 5 });
        this.sendTo(4, 'doScene', { scene: 5 });
      }
    });

    this.time.delayedCall(
      4000,
      () => {
        this.sendTo(1, 'doScene', { scene: 5 });
        this.sendTo(5, 'doScene', { scene: 5 });
      },
      [],
      this
    );
  }

  showScene4() {
    this.sendTo(1, 'doScene', { scene: 6 });
    this.time.delayedCall(
      1000,
      () => {
        this.sendTo(2, 'doScene', { scene: 6 });
      },
      [],
      this
    );

    this.time.delayedCall(
      2000,
      () => {
        this.gift1.setPosition(-200, Utils.GlobalSettings.height / 2);
        this.gift2.setPosition(-300, Utils.GlobalSettings.height / 2 + 100);
        this.gift3.setPosition(-250, Utils.GlobalSettings.height / 2 - 100);
        this.gift4.setPosition(-400, Utils.GlobalSettings.height / 2);

        this.gift1.visible = true;
        this.gift2.visible = true;
        this.gift3.visible = true;
        this.gift4.visible = true;

        this.tweens.add({
          targets: [this.gift1, this.gift2, this.gift3, this.gift4],
          x: Utils.GlobalSettings.width + 200,
          angle: 360 * 1,
          ease: 'Sine.easeOut',
          duration: 1000
        });
      },
      [],
      this
    );

    this.time.delayedCall(
      3000,
      () => {
        this.sendTo(4, 'doScene', { scene: 6 });
      },
      [],
      this
    );

    this.time.delayedCall(
      4000,
      () => {
        this.sendTo(5, 'doScene', { scene: 6 });
      },
      [],
      this
    );
  }

  showScene5() {
    this.cameras.main.flash(200);
    this.bg.setTint(0x27ae60);

    this.tree.setPosition(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height + 320
    );
    this.tree.visible = true;

    this.tweens.add({
      targets: [this.tree],
      y: Utils.GlobalSettings.height - 100,
      ease: 'Sine.easeOut',
      duration: 2000
    });

    this.sendTo(1, 'doScene', { scene: 7 });
    this.sendTo(2, 'doScene', { scene: 7 });
    this.sendTo(4, 'doScene', { scene: 7 });
    this.sendTo(5, 'doScene', { scene: 7 });
  }

  showScene6() {
    this.showStockings = true;
    this.stockingGroup.setVisible(true);
    this.stockingTween = this.tweens.addCounter({
      from: 220,
      to: 100,
      duration: 3000,
      delay: 2000,
      ease: 'Sine.easeInOut',
      repeat: -1,
      yoyo: true
    });

    this.time.delayedCall(
      2000,
      () => {
        this.sendTo(2, 'doScene', { scene: 8 });
        this.sendTo(4, 'doScene', { scene: 8 });
      },
      [],
      this
    );

    this.time.delayedCall(
      4000,
      () => {
        this.sendTo(1, 'doScene', { scene: 8 });
        this.sendTo(5, 'doScene', { scene: 8 });
      },
      [],
      this
    );
  }

  showScene7() {
    this.sendTo(5, 'doScene', { scene: 9 });
    this.time.delayedCall(
      1000,
      () => {
        this.sendTo(4, 'doScene', { scene: 9 });
      },
      [],
      this
    );

    this.time.delayedCall(
      2000,
      () => {
        this.santa.setPosition(
          Utils.GlobalSettings.width + 283,
          Utils.GlobalSettings.height / 2
        );

        this.santa.visible = true;

        this.tweens.add({
          targets: [this.santa],
          x: -283,
          ease: 'Linear.easeOut',
          duration: 1000
        });
      },
      [],
      this
    );

    this.time.delayedCall(
      3000,
      () => {
        this.sendTo(2, 'doScene', { scene: 9 });
      },
      [],
      this
    );

    this.time.delayedCall(
      4000,
      () => {
        this.sendTo(1, 'doScene', { scene: 9 });
      },
      [],
      this
    );
  }

  showScene8() {
    this.cameras.main.flash(200);
    this.bg.setTint(0xf39c12);
  }

  showScene9() {
    this.cameras.main.flash(200, 255, 255, 255, true);
    this.merry.visible = true;
    this.merry.alpha = 0;
    this.merry.setScale(0);

    this.time.addEvent({
      delay: 200,
      callback: () => {
        this.cameras.main.flash(200, 255, 255, 255, true);
      },
      callbackScope: this,
      repeat: 6
    });

    this.tweens.add({
      targets: [this.merry],
      scaleX: 1,
      scaleY: 1,
      alpha: 1,
      ease: 'Back.easeOut',
      duration: 2000
    });
  }

  getScreen(order) {
    for (var i = 0; i < this.screens.length; i++) {
      if (this.screens[i].order === order) {
        return this.screens[i];
      }
    }
    return null;
  }

  sendTo(screenOrder, type, msg) {
    var screen = this.getScreen(screenOrder);
    if (screen) {
      screen.send(type, msg);
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  start() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      // Utils.GlobalSettings.bgm.setVolume();
      Utils.GlobalSettings.bgm.setLoop(false);
      Utils.GlobalSettings.bgm.play();

      this.songTime = this.time.delayedCall(101696, this.songDone, [], this);
      this.started = true;
    }
  }

  songDone() {
    console.log('done');
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        //Utils.GlobalSettings.bgm.pause();
      } else {
        //Utils.GlobalSettings.bgm.resume();
      }
    }
  }

  inTimeRange(id, elapsedTime, myTime, tolerance = 200) {
    if (!this.beatPoints[id]) {
      var offset = myTime - elapsedTime;
      if (Math.abs(offset) < tolerance) {
        this.beatPoints[id] = true;
        return true;
      }
    }

    return false;
  }

  processTimer() {
    var time = this.songTime.getElapsed();

    this.sendTo(1, 'timer', { timer: time });
    this.sendTo(2, 'timer', { timer: time });
    this.sendTo(3, 'timer', { timer: time });
    this.sendTo(4, 'timer', { timer: time });

    //intro part 1:
    if (this.inTimeRange(1, time, 7094.141333333374)) {
      this.sendTo(1, 'doScene', { scene: 1 });
    } else if (this.inTimeRange(2, time, 12541.766833333373)) {
      this.sendTo(5, 'doScene', { scene: 1 });
    } else if (this.inTimeRange(3, time, 16939.284333333373)) {
      this.sendTo(2, 'doScene', { scene: 1 });
    } else if (this.inTimeRange(4, time, 20624.45483333342)) {
      this.sendTo(4, 'doScene', { scene: 1 });
    }

    //intro part 2:
    if (this.inTimeRange(5, time, 24312.48783333335)) {
      this.sendTo(1, 'doScene', { scene: 2 });
      this.sendTo(5, 'doScene', { scene: 2 });
    } else if (this.inTimeRange(6, time, 27459.415333333356)) {
      this.sendTo(2, 'doScene', { scene: 2 });
    } else if (this.inTimeRange(7, time, 28478.162833333336)) {
      this.sendTo(4, 'doScene', { scene: 2 });
    } else if (this.inTimeRange(8, time, 31153.265333333344)) {
      this.sendTo(1, 'doScene', { scene: 2 });
    } else if (this.inTimeRange(9, time, 32810.69033333336)) {
      this.sendTo(5, 'doScene', { scene: 2 });
    }

    //intro part 3:
    if (this.inTimeRange(10, time, 34847.1675, 100)) {
      this.showScene1();
    } else if (this.inTimeRange(11, time, 38767.948833333394)) {
      this.sendTo(2, 'doScene', { scene: 3 });
      this.sendTo(4, 'doScene', { scene: 3 });
    } else if (this.inTimeRange(12, time, 41661.749333333435)) {
      this.sendTo(1, 'doScene', { scene: 3 });
      this.sendTo(5, 'doScene', { scene: 3 });
    }

    //snowfall + flashing 3
    //snowfall + flashing 2,4
    //snowfall + flashing 1,5
    if (this.inTimeRange(13, time, 50000.897)) {
      this.showSnow();
    } else if (this.inTimeRange(14, time, 50579.897)) {
      this.showScene2();
    } else if (this.inTimeRange(15, time, 52476.044833333486)) {
      this.sendTo(2, 'doScene', { scene: 4 });
      this.sendTo(4, 'doScene', { scene: 4 });
    } else if (this.inTimeRange(16, time, 54061.91433333346)) {
      this.sendTo(1, 'doScene', { scene: 4 });
      this.sendTo(5, 'doScene', { scene: 4 });
    }

    //background color blue:
    //scale out from middle 3
    //scale out from right 2, scale out from left 4
    //scale out from right 1, scale out from left 5
    if (this.inTimeRange(17, time, 56961.847666666494)) {
      this.showScene3();
    }

    //63790.675
    //presents bouncing from right to left
    if (this.inTimeRange(18, time, 63790.675)) {
      this.showScene4();
    }

    //68267.73099999983
    //christmas tree popup 1,5
    //christmas tree popup 2,4
    //christmas tree popup 3
    if (this.inTimeRange(19, time, 68267.73099999983)) {
      this.showScene5();
    }

    //70109.06150000019
    //christmas tree move down, stocking fall down 3
    //christmas tree move down, stocking fall down 2,4
    //christmas tree move down, stocking fall down 1,5
    if (this.inTimeRange(20, time, 70109.06150000019)) {
      this.showScene6();
    }

    //76922.834
    //santa fly in from left to right along path
    if (this.inTimeRange(21, time, 76922.834)) {
      this.showScene7();
    }

    //82857.56849999992
    //ending
    if (this.inTimeRange(22, time, 82857.56849999992)) {
      this.sendTo(1, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(23, time, 83857.56849999992)) {
      this.sendTo(2, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(24, time, 84857.56849999992)) {
      this.showScene8();
    } else if (this.inTimeRange(25, time, 85857.56849999992)) {
      this.sendTo(4, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(26, time, 86857.56849999992)) {
      this.sendTo(5, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(27, time, 87857.56849999992)) {
      this.sendTo(4, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(28, time, 88857.56849999992)) {
      this.showScene8();
    } else if (this.inTimeRange(29, time, 89857.56849999992)) {
      this.sendTo(2, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(30, time, 90857.56849999992)) {
      this.sendTo(1, 'doScene', { scene: 10 });
    }

    //ending 2
    if (this.inTimeRange(31, time, 91857.56849999992)) {
      this.showScene8();
    } else if (this.inTimeRange(32, time, 92857.56849999992)) {
      this.sendTo(2, 'doScene', { scene: 10 });
      this.sendTo(4, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(33, time, 93857.56849999992)) {
      this.sendTo(1, 'doScene', { scene: 10 });
      this.sendTo(5, 'doScene', { scene: 10 });
    } else if (this.inTimeRange(34, time, 94857.56849999992)) {
      this.sendTo(2, 'doScene', { scene: 10 });
      this.sendTo(4, 'doScene', { scene: 10 });
    }

    //finale
    if (this.inTimeRange(35, time, 95857.56849999992)) {
      this.showScene9();
    } else if (this.inTimeRange(36, time, 96857.56849999992)) {
      this.sendTo(2, 'doScene', { scene: 11 });
      this.sendTo(4, 'doScene', { scene: 11 });
    } else if (this.inTimeRange(37, time, 97857.56849999992)) {
      this.sendTo(1, 'doScene', { scene: 11 });
      this.sendTo(5, 'doScene', { scene: 11 });
    }
  }

  update(time, delta) {
    if (this.songTime) {
      this.timeText.setText('time: ' + this.songTime.getElapsed().toString());

      this.processTimer();
    }

    if (this.snowFall && this.snowPool) {
      this.snowPool.children.each(this.updateSnow.bind(this));
    }

    if (this.showStockings && this.stockingGroup) {
      Phaser.Actions.RotateAroundDistance(
        this.stockingGroup.getChildren(),
        {
          x: Utils.GlobalSettings.width / 2,
          y: Utils.GlobalSettings.height / 2
        },
        0.02,
        this.stockingTween.getValue()
      );
    }
  }

  shutdown() {
    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill game');
  }
}
