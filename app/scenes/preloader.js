import Utils from 'utils/utils';

export default class Preloader extends Phaser.Scene {
  constructor() {
    super('preloader');
    this.progressBar = null;
    this.progressBarRectangle = null;
  }

  preload() {
    this.load.setCORS('Anonymous');

    if (Utils.GlobalSettings.isController) {
      this.load.atlas(
        'atlas1',
        'assets/controller-0.png',
        'assets/controller-0.json'
      );
    } else {
      this.load.atlas(
        'atlas1',
        'assets/sprites-0.png',
        'assets/sprites-0.json'
      );

      this.load.audio('bgm', ['assets/audio/bgm.ogg', 'assets/audio/bgm.mp3']);
      this.load.audio('gp', ['assets/audio/gp.ogg', 'assets/audio/gp.m4a']);
      this.load.audio('eat', ['assets/audio/eat.ogg']);
      this.load.audio('join', ['assets/audio/join.ogg']);
      this.load.audio('find', ['assets/audio/find.ogg']);
      this.load.audio('speed', ['assets/audio/speed.ogg']);
      this.load.audio('slow', ['assets/audio/slow.ogg']);
      this.load.audio('invincible', ['assets/audio/invincible.ogg']);
      this.load.audio('freeze', ['assets/audio/freeze.ogg']);
      this.load.audio('die', ['assets/audio/die.ogg']);
      this.load.audio('start', ['assets/audio/start.ogg']);
      this.load.audio('time', ['assets/audio/time.ogg']);
      this.load.audio('click', [
        'assets/audio/click.ogg',
        'assets/audio/click.m4a'
      ]);
    }

    this.load.image('bg', 'assets/bg.png');

    this.load.bitmapFont(
      'modak',
      'assets/fonts/modak_0.png',
      'assets/fonts/modak.xml'
    );

    this.load.text('text', 'assets/text.json');

    this.load.on('progress', this.onLoadProgress, this);
    this.load.on('complete', this.onLoadComplete, this);
    this.createProgressBar();

    this.icon = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'logo'
    );
    this.icon.setOrigin(0.5, 0.5);
    this.icon.setInteractive();
  }

  create() {
    Utils.load();

    this.message = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      this.icon.y + 100,
      'modak',
      'Click to Play!',
      50
    );
    this.message.setOrigin(0.5, 0.5);
    this.message.setVisible(false);

    if (Utils.GlobalSettings.isController) {
      this.message.setVisible(true);

      this.icon.on('pointerup', () => {
        this.scale.startFullscreen();
        this.transitionOut('controllerScene');
      });

      // this.input.on('pointerdown', () => {
      //   this.scale.startFullscreen();
      //   this.transitionOut('controllerScene');
      // });
    } else {
      Utils.GlobalSettings.bgm = this.sound.add('bgm');
      this.gpSnd = this.sound.add('gp');

      this.message.setVisible(true);

      this.input.on('pointerdown', () => {
        // this.gpSnd.play();
        this.transitionOut('gameScene');
      });
    }

    this.events.on('shutdown', this.shutdown, this);
  }

  transitionOut(target = 'gameScene') {
    var tween = this.tweens.add({
      targets: [this.icon, this.message],
      alpha: 0,
      ease: 'Power1',
      duration: 1000,
      onComplete: () => {
        this.scene.start(target, { from: 'preloader' });
      }
    });
  }

  // extend:

  createProgressBar() {
    var main = this.cameras.main;
    this.progressBarRectangle = new Phaser.Geom.Rectangle(
      0,
      0,
      0.75 * main.width,
      20
    );
    Phaser.Geom.Rectangle.CenterOn(
      this.progressBarRectangle,
      0.5 * main.width,
      main.height - 150
    );
    this.progressBar = this.add.graphics();
  }

  onLoadComplete(loader) {
    //console.log('onLoadComplete', loader);
    this.progressBar.destroy();
  }

  onLoadProgress(progress) {
    var rect = this.progressBarRectangle;
    var color = 0xca59ff;
    this.progressBar
      .clear()
      .fillStyle(0x222222)
      .fillRect(rect.x, rect.y, rect.width, rect.height)
      .fillStyle(color)
      .fillRect(rect.x, rect.y, progress * rect.width, rect.height);
    //console.log('progress', progress);
  }

  update() {}

  shutdown() {
    if (this.icon) {
      this.icon.destroy();
      this.icon = null;
    }

    if (this.message) {
      this.message.destroy();
      this.message = null;
    }

    if (this.gpSnd) {
      this.gpSnd.destroy();
      this.gpSnd = null;
    }

    if (this.progressBarRectangle) {
      this.progressBarRectangle = null;
    }

    this.events.off('shutdown', this.shutdown, this);

    console.log('kill preloader');
  }
}
