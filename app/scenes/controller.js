import Utils from 'utils/utils';

export default class ControllerScene extends Phaser.Scene {
  constructor() {
    super('controllerScene');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    this.bg = this.add.image(0, 0, 'bg');
    this.bg.setOrigin(0);
    this.bg.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bg.setTint(0x331188);

    this.setupSnow();

    this.timeText = this.add.text(32, 32);
    this.timeText.visible = false;

    this.client = new HFT.GameClient();
    this.client.on('connect', this.handleConnect.bind(this));
    this.client.on('disconnect', this.handleDisconnect.bind(this));
    this.client.on('doScene', this.handleScenes.bind(this));
    this.client.on('timer', cmd => {
      this.timeText.setText('time: ' + cmd.timer);
    });

    this.order = 0;

    this.setupOrderScene();
    this.setupScene1();
    this.setupScene6();
    this.setupScene7();
    this.setupScene8();
    this.setupScene9();
    this.setupScene11();
  }

  setupOrderScene() {
    this.orderText = this.add.bitmapText(
      10,
      5,
      'modak',
      'Select Screen Order',
      30
    );

    this.btn1 = this.add
      .image(
        Utils.GlobalSettings.width / 2 - 300,
        Utils.GlobalSettings.height / 2,
        'atlas1',
        '1'
      )
      .setInteractive();

    this.btn2 = this.add
      .image(
        Utils.GlobalSettings.width / 2 - 100,
        Utils.GlobalSettings.height / 2,
        'atlas1',
        '2'
      )
      .setInteractive();

    this.btn3 = this.add
      .image(
        Utils.GlobalSettings.width / 2 + 100,
        Utils.GlobalSettings.height / 2,
        'atlas1',
        '4'
      )
      .setInteractive();

    this.btn4 = this.add
      .image(
        Utils.GlobalSettings.width / 2 + 300,
        Utils.GlobalSettings.height / 2,
        'atlas1',
        '5'
      )
      .setInteractive();

    this.btn1.on('pointerdown', pointer => this.sendSetOrder(1), this);
    this.btn2.on('pointerdown', pointer => this.sendSetOrder(2), this);
    this.btn3.on('pointerdown', pointer => this.sendSetOrder(4), this);
    this.btn4.on('pointerdown', pointer => this.sendSetOrder(5), this);
  }

  setupScene1() {
    this.bulb = this.add.sprite(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'atlas1',
      'light1'
    );

    this.bulb.visible = false;
  }

  setupScene6() {
    this.gift1 = this.add.sprite(0, 0, 'atlas1', 'gift1');
    this.gift2 = this.add.sprite(0, 0, 'atlas1', 'gift2');
    this.gift3 = this.add.sprite(0, 0, 'atlas1', 'gift3');
    this.gift4 = this.add.sprite(0, 0, 'atlas1', 'gift4');

    this.gift1.visible = false;
    this.gift2.visible = false;
    this.gift3.visible = false;
    this.gift4.visible = false;
  }

  setupScene7() {
    this.tree = this.add.image(0, 0, 'atlas1', 'tree');
    this.tree.visible = false;
  }

  setupScene8() {
    this.showStockings = false;
    this.stockingCircle = new Phaser.Geom.Circle(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      350
    );

    this.stockingGroup = this.add.group({
      key: 'atlas1',
      frame: ['stocking1', 'stocking2', 'stocking3'],
      repeat: 5
    });

    Phaser.Actions.PlaceOnCircle(
      this.stockingGroup.getChildren(),
      this.stockingCircle
    );

    this.stockingGroup.setVisible(false);
  }

  setupScene9() {
    this.santa = this.add.image(0, 0, 'atlas1', 'santa');
    this.santa.visible = false;
  }

  setupScene11() {
    this.merry = this.add.image(
      Utils.GlobalSettings.width / 2,
      150,
      'atlas1',
      'merry'
    );

    this.merry.visible = false;
  }

  showScene1() {
    var bgRGB = { r: 255, g: 255, b: 255 }; //this.getBGColorRGB();
    this.cameras.main.flash(500, bgRGB.r, bgRGB.g, bgRGB.b);
    this.bg.setTint(this.getBGColor());
    this.bulb.setFrame('light' + this.order);
    this.bulb.visible = true;
    this.bulb.setScale(0);
    this.bulb.alpha = 0;

    this.tweens.add({
      targets: [this.bulb],
      scaleX: 1,
      scaleY: 1,
      alpha: 1,
      ease: 'Back.easeOut',
      duration: 500
    });
  }

  hideScene1() {
    this.bulb.visible = false;
  }

  showScene2() {
    var bgRGB = { r: 255, g: 255, b: 255 }; //this.getBGColorRGB();
    this.cameras.main.flash(1000, bgRGB.r, bgRGB.g, bgRGB.b);
  }

  showScene3() {
    var bgRGB = this.getBGColorRGB();
    this.cameras.main.flash(1000, bgRGB.r, bgRGB.g, bgRGB.b);

    this.bg.alpha = 1;
    this.bulb.visible = true;
    this.bulb.alpha = 1;
    this.bulb.setFrame('light6');

    this.tweens.add({
      targets: [this.bg, this.bulb],
      alpha: 0,
      ease: 'Linear.easeOut',
      duration: 1500
    });
  }

  showScene4() {
    this.showSnow();
    var bgRGB = this.getBGColorRGB();
    this.cameras.main.flash(200, bgRGB.r, bgRGB.g, bgRGB.b, true);

    this.time.addEvent({
      delay: 200,
      callback: () => {
        this.cameras.main.flash(200, bgRGB.r, bgRGB.g, bgRGB.b, true);
      },
      callbackScope: this,
      repeat: this.order === 2 || this.order === 4 ? 12 : 8
    });
  }

  showScene5() {
    this.bg.setTint(0x3498db);
    this.bg.visible = true;
    this.bg.alpha = 0;
    if (this.order === 1 || this.order === 2) {
      this.bg.setOrigin(1, 0);
      this.bg.x = Utils.GlobalSettings.width;
    } else if (this.order === 4 || this.order === 5) {
      this.bg.setOrigin(0, 0);
      this.bg.x = 0;
    }

    var startingScale = this.bg.scaleX;
    this.bg.scaleX = 0;

    this.tweens.add({
      targets: [this.bg],
      alpha: 1,
      scaleX: startingScale,
      ease: 'Sine.easeOut',
      duration: 2000
    });
  }

  showScene6() {
    this.gift1.setPosition(-200, Utils.GlobalSettings.height / 2);
    this.gift2.setPosition(-300, Utils.GlobalSettings.height / 2 + 100);
    this.gift3.setPosition(-250, Utils.GlobalSettings.height / 2 - 100);
    this.gift4.setPosition(-400, Utils.GlobalSettings.height / 2);

    this.gift1.visible = true;
    this.gift2.visible = true;
    this.gift3.visible = true;
    this.gift4.visible = true;

    this.tweens.add({
      targets: [this.gift1, this.gift2, this.gift3, this.gift4],
      x: Utils.GlobalSettings.width + 200,
      angle: 360 * 1,
      ease: 'Sine.easeOut',
      duration: 1000
    });
  }

  showScene7() {
    this.cameras.main.flash(200);
    this.bg.setTint(0x27ae60);

    this.tree.setPosition(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height + 320
    );
    this.tree.visible = true;

    this.tweens.add({
      targets: [this.tree],
      y: Utils.GlobalSettings.height - 100,
      ease: 'Sine.easeOut',
      duration: 2000
    });
  }

  showScene8() {
    this.showStockings = true;
    this.stockingGroup.setVisible(true);
    this.stockingTween = this.tweens.addCounter({
      from: 220,
      to: 100,
      duration: 3000,
      delay: 2000,
      ease: 'Sine.easeInOut',
      repeat: -1,
      yoyo: true
    });
  }

  showScene9() {
    this.santa.setPosition(
      Utils.GlobalSettings.width + 283,
      Utils.GlobalSettings.height / 2
    );

    this.santa.visible = true;

    this.tweens.add({
      targets: [this.santa],
      x: -283,
      ease: 'Linear.easeOut',
      duration: 1000
    });
  }

  showScene10() {
    var bgRGB = { r: 255, g: 255, b: 255 }; //this.getBGColorRGB();
    this.cameras.main.flash(200, bgRGB.r, bgRGB.g, bgRGB.b, true);
    this.bg.setTint(this.getBGColor());
  }

  showScene11() {
    this.cameras.main.flash(200, 255, 255, 255, true);
    this.merry.visible = true;
    this.merry.alpha=0;
    this.merry.setScale(0);

    this.time.addEvent({
      delay: 200,
      callback: () => {
        this.cameras.main.flash(200, 255, 255, 255, true);
      },
      callbackScope: this,
      repeat: 6
    });

    this.tweens.add({
      targets: [this.merry],
      scaleX: 1,
      scaleY: 1,
      alpha: 1,
      ease: 'Back.easeOut',
      duration: 2000
    });
  }

  setupSnow() {
    this.snowFall = false;

    this.snowPool = this.add.group();
    for (var i = 0; i < 20; i++) {
      var spr = this.snowPool.create(0, 0, 'atlas1', 'flake1');
      spr.alpha = Phaser.Math.RND.realInRange(0.5, 1);
      spr._xspeed = Phaser.Math.RND.realInRange(-0.3, 0.3);
      spr._yspeed = Phaser.Math.RND.realInRange(1.2, 1.4);
      spr._curYSpeed = 0;
      spr.setFrame('flake' + Phaser.Math.RND.integerInRange(1, 9));
      spr.setScale(Phaser.Math.RND.realInRange(0.3, 1));
      spr.x = Phaser.Math.RND.integerInRange(0, Utils.GlobalSettings.width);

      if (i < 10) {
        spr.y = Phaser.Math.RND.integerInRange(-100, -200);
      } else {
        spr.y = Phaser.Math.RND.integerInRange(
          -200,
          -Utils.GlobalSettings.height
        );
      }
    }
    this.snowPool.setVisible(false);
  }

  updateSnow(sprite) {
    if (sprite) {
      if (sprite.y > Utils.GlobalSettings.height + 100) {
        sprite.x = Phaser.Math.RND.integerInRange(
          0,
          Utils.GlobalSettings.width
        );

        sprite.y = Phaser.Math.RND.integerInRange(
          -100,
          -Utils.GlobalSettings.height
        );

        sprite.setFrame('flake' + Phaser.Math.RND.integerInRange(1, 9));
        sprite.alpha = Phaser.Math.RND.realInRange(0.5, 1);
        sprite._xspeed = Phaser.Math.RND.realInRange(-0.3, 0.3);
        sprite._yspeed = Phaser.Math.RND.realInRange(1.2, 1.4);
        sprite.setScale(Phaser.Math.RND.realInRange(0.3, 1));
      } else {
        sprite.x += sprite._xspeed;
        if (sprite.alpha > 0) {
          sprite.alpha -= 0.0002;
        }

        sprite.angle += sprite._xspeed;
        if (sprite.angle > 360) {
          sprite.angle = 0;
        }

        if (sprite._curYSpeed < sprite._yspeed) {
          sprite._curYSpeed += 0.01;
        } else {
          sprite._curYSpeed = sprite._yspeed;
        }

        sprite.y += sprite._curYSpeed;
      }
    }
  }

  showSnow() {
    this.snowFall = true;
    this.snowPool.setVisible(true);
  }

  getBGColor() {
    switch (this.order) {
      case 1:
        return 0xe74c3c;
        break;

      case 2:
        return 0x27ae60;
        break;

      case 4:
        return 0x2980b9;
        break;

      case 5:
        return 0x8e44ad;
        break;
    }
  }

  getBGColorRGB() {
    switch (this.order) {
      case 1:
        return { r: 231, g: 76, b: 60 };
        break;

      case 2:
        return { r: 39, g: 174, b: 96 };
        break;

      case 4:
        return { r: 41, g: 128, b: 185 };
        break;

      case 5:
        return { r: 142, g: 68, b: 173 };
        break;
    }
  }

  sendSetOrder(order) {
    this.order = order;

    this.client.sendCmd('order', {
      order: order
    });

    this.hideScreenOrder();
    this.bg.setTint(0x000000);
  }

  showScreenOrder() {
    this.btn1.visible = true;
    this.btn2.visible = true;
    this.btn3.visible = true;
    this.btn4.visible = true;
    this.orderText.visible = true;
  }

  hideScreenOrder() {
    this.btn1.visible = false;
    this.btn2.visible = false;
    this.btn3.visible = false;
    this.btn4.visible = false;
    this.orderText.visible = false;
  }

  handleConnect() {}

  handleDisconnect() {}

  handleScenes(cmd) {
    switch (cmd.scene) {
      case 1:
        this.showScene1();
        break;

      case 2:
        this.showScene2();
        break;

      case 3:
        this.showScene3();
        break;

      case 4:
        this.showScene4();
        break;

      case 5:
        this.showScene5();
        break;

      case 6:
        this.showScene6();
        break;

      case 7:
        this.showScene7();
        break;

      case 8:
        this.showScene8();
        break;

      case 9:
        this.showScene9();
        break;

      case 10:
        this.showScene10();
        break;

      case 11:
        this.showScene11();
        break;
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        //Utils.GlobalSettings.bgm.pause();
      } else {
        //Utils.GlobalSettings.bgm.resume();
      }
    }
  }

  update(time, delta) {
    if (this.snowFall && this.snowPool) {
      this.snowPool.children.each(this.updateSnow.bind(this));
    }

    if (this.showStockings && this.stockingGroup) {
      Phaser.Actions.RotateAroundDistance(
        this.stockingGroup.getChildren(),
        {
          x: Utils.GlobalSettings.width / 2,
          y: Utils.GlobalSettings.height / 2
        },
        0.02,
        this.stockingTween.getValue()
      );
    }
  }

  shutdown() {
    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill controller');
  }
}
