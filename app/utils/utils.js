let globalSettings = {
  namespace: 'xmas2019',
  width: 1024,
  height: 640,
  adReady: false,
  text: null,
  mode: 0,
  version: '0.0.1',
  bgm: null,
  localIPAddress: undefined,
  port: 15151,
  isController: false,
  orientationListener: undefined
};

let savedSettings = {
  muted: false,
  lastNickname: '',
  firstTime: true,
  isMobile: false,
  continuing: false,
  timesPlayed: 0
};

export default class Utils {
  constructor() {}

  static setup() {
    var path = window.location.pathname;
    var page = path.split('/').pop();
    if (page === 'controller.html') {
      globalSettings.width = 1280;
      globalSettings.height = 640;
      globalSettings.isController = true;
    } else {
      globalSettings.width = 1440;
      globalSettings.height = 672;
      globalSettings.isController = false;
      this.findLocalIPAddress();
    }
  }

  static get GlobalSettings() {
    return globalSettings;
  }

  static get SavedSettings() {
    return savedSettings;
  }

  static save() {
    localStorage[globalSettings.namespace + '.saveData'] = JSON.stringify(
      savedSettings
    );
  }

  static load() {
    if (localStorage[globalSettings.namespace + '.saveData']) {
      savedSettings = JSON.parse(
        localStorage[globalSettings.namespace + '.saveData']
      );
    }
  }

  static findLocalIPAddress() {
    //find LAN ip address
    window.RTCPeerConnection =
      window.RTCPeerConnection ||
      window.mozRTCPeerConnection ||
      window.webkitRTCPeerConnection; //compatibility for Firefox and chrome

    if (window.RTCPeerConnection) {
      var pc = new RTCPeerConnection({ iceServers: [] }),
        noop = function() {};

      if (pc) {
        pc.createDataChannel(''); //create a bogus data channel
        pc.createOffer(pc.setLocalDescription.bind(pc), noop); // create offer and set local description
        pc.onicecandidate = function(ice) {
          if (ice && ice.candidate && ice.candidate.candidate) {
            var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(
              ice.candidate.candidate
            )[1];
            globalSettings.localIPAddress = myIP;
            //console.log('my IP: ', myIP);
            pc.onicecandidate = noop;
            pc.close();
            pc = null;
          }
        };
      }
    }
  }

  static hexColorToInteger(color) {
    color = color.replace('#', '0x');
    return parseInt(color);
  }

  static checkOrientation() {
    // Find matches
    var mql = window.matchMedia('(orientation: portrait)');

    // If there are matches, we're in portrait
    if (mql.matches) {
      // Portrait orientation
      if (globalSettings.orientationListener) {
        if (
          typeof globalSettings.orientationListener.orientationChanged ===
          'function'
        ) {
          globalSettings.orientationListener.orientationChanged('portrait');
        }
        $('#turn').hide();
      } else {
        $('#turn').insertAfter('canvas');
        $('#turn').show();
      }
    } else {
      // Landscape orientation
      if (globalSettings.orientationListener) {
        if (
          typeof globalSettings.orientationListener.orientationChanged ===
          'function'
        ) {
          globalSettings.orientationListener.orientationChanged('landscape');
        }
        $('#turn').hide();
      } else {
        $('#turn').hide();
      }
    }

    // Add a media query change listener
    mql.addListener(m => {
      if (m.matches) {
        // Changed to portrait
        if (globalSettings.orientationListener) {
          if (
            typeof globalSettings.orientationListener.orientationChanged ===
            'function'
          ) {
            globalSettings.orientationListener.orientationChanged('portrait');
          }
          $('#turn').hide();
        } else {
          $('#turn').insertAfter('canvas');
          $('#turn').show();
        }
      } else {
        // Changed to landscape
        if (globalSettings.orientationListener) {
          if (
            typeof globalSettings.orientationListener.orientationChanged ===
            'function'
          ) {
            globalSettings.orientationListener.orientationChanged('landscape');
          }
          $('#turn').hide();
        } else {
          $('#turn').hide();
        }
      }
    });
  }
}
