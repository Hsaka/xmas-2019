import Utils from '../utils/utils';

export default class Screen {
  constructor(scene, netPlayer, id) {
    this.netPlayer = netPlayer;
    this.scene = scene;
    this.id = id;

    this.order = 0;

    if (netPlayer) {
      netPlayer.addEventListener('disconnect', this.disconnect.bind(this));
      netPlayer.addEventListener('order', this.setOrder.bind(this));
    }
  }

  disconnect() {
    console.log('disconnected');
  }

  setOrder(cmd) {
    this.order = cmd.order;
    console.log(this.order);
  }

  send(type, msg) {
    if (this.netPlayer) {
      this.netPlayer.sendCmd(type, msg);
    }
  }

  destroy() {
    if (this.netPlayer) {
      this.netPlayer.removeAllListeners();
      this.netPlayer = null;
    }
  }
}
