# http://brunch.io/docs/config
module.exports =
  conventions:
    assets: 'app/static/**'
  files:
    javascripts:
      joinTo:
        'app.js': 'app/**'
        'vendor.js': ['node_modules/**', 'vendor/**']
    stylesheets:
      joinTo: 'app.css'
  modules:
    autoRequire:
      'app.js': ['initialize']
  npm:
    static: [
      'vendor/phaser.min.js',
      'vendor/jquery.min.js',
      'vendor/moment.min.js',
      'vendor/sweetalert2.min.js'
      # 'node_modules/phaser/dist/phaser-arcade-physics.js'
    ]
  plugins:
    # https://github.com/babel/babel-brunch#configuration
    babel:
      ignore: ['node_modules/**', 'vendor/**']
    replacement: 
      replacements: [
        files: [/\app.js$/],
        match: 
          find: /(?!_)\b(require)(?!_|d)/gm, replace: 'brunchRequire'
      ]
    afterBrunch: [
      # 'node transformer.js'
      'copy main.js public'
    ]    
  server:
    noPushState: on
